// Copyright © 2017 Hardik Bagdi <hbagdi1@binghamton.edu>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package github

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
)

type authRequest struct {
	Note         string   `json:"note"`
	NoteURL      string   `json:"note_url,omitempty"`
	ClientID     string   `json:"client_id"`
	ClientSecret string   `json:"client_secret"`
	Scopes       []string `json:"scopes"`
}

const (
	githubAuthEndpoint = "https://api.github.com/authorizations"
)

var (
	authReq authRequest
)

func init() {
	authReq.Note = "Joe is simple."
	authReq.Scopes = []string{"public_repo"}
	clientid, ok := os.LookupEnv("github_clientid")
	if ok {
		authReq.ClientID = clientid
	} else {
		fmt.Println("github clientid is not set")
		os.Exit(1)
	}
	clientsecret, ok := os.LookupEnv("github_secret")
	if ok {
		authReq.ClientSecret = clientsecret
	} else {
		fmt.Println("github secret is not set")
		os.Exit(1)
	}
}
func buildBasicAuthRequest(username, password string) (*http.Request, error) {
	b := new(bytes.Buffer)
	err := json.NewEncoder(b).Encode(authReq)
	if err != nil {
		return nil, err
	}
	request, err := http.NewRequest("POST", githubAuthEndpoint, b)
	if err != nil {
		return nil, err
	}
	request.Header.Set("Content-Type", "application/json")
	request.SetBasicAuth(username, password)
	return request, nil
}

func tokenFromResponse(res *http.Response) (map[string]interface{}, error) {
	if res.StatusCode != 201 {
		return nil, errors.New("Incorrect username/password.")
	}

	body, _ := ioutil.ReadAll(res.Body)
	var jsonResponse interface{}
	err := json.Unmarshal(body, &jsonResponse)
	if err != nil {
		return nil, err
	}
	return jsonResponse.(map[string]interface{}), nil
}

// BasicAuthAPIToken return a API token for the user account
func BasicAuthAPIToken(username, password string) (string, error) {
	//build our request for API token
	request, err := buildBasicAuthRequest(username, password)
	if err != nil {
		return "", err
	}
	client := &http.Client{}
	//post the request
	response, err := client.Do(request)
	if err != nil {
		return "", err
	}
	defer response.Body.Close()
	responseMap, err := tokenFromResponse(response)
	if err != nil {
		return "", err
	}
	token, ok := responseMap["token"].(string)
	if !ok {
		return "", errors.New("Joe could not set things up.")
	}
	if token == "" {
		return "", errors.New("Joe could not set things up.")
	}
	return token, nil
}
