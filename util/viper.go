// Copyright © 2017 Hardik Bagdi <hbagdi1@binghamton.edu>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package util

import (
	"errors"
	"fmt"
	"os"

	"encoding/json"

	"github.com/spf13/viper"
)

var (
	// CfgFile is the configuration file used for viper
	cfgFile        = ".joe"
	cfgFileWithExt = cfgFile + ".json"
	// CfgPath is the directory containing the CfgFile
	cfgPath        = ""
	apiTokenString = "token"
	appConfig      config
	viperC         *viper.Viper
)

//Config represents configurations of joe
type config struct {
	Token string `json:"token"`
}

func init() {
	cfgPath = os.Getenv("HOME") + "/"
	viperC = viper.New()
	viperC.SetConfigName(cfgFile)
	viperC.AddConfigPath(cfgPath)
}

// SetAPIToken sets the key as the API key in viper and config file
func SetAPIToken(token string) error {
	if token == "" {
		return errors.New("API key cannot be null")
	}
	appConfig.Token = token
	viperC.Set(apiTokenString, token)
	return nil
}

// Token returns the key from the configuration
func Token() (string, error) {
	if appConfig.Token == "" {
		return "", errors.New("API key not set")
	}
	return appConfig.Token, nil
}

// LoadViperConfig loads viper configuration from the cfgFile
func LoadViperConfig() error {
	err := viperC.ReadInConfig()
	if err != nil {
		err = errors.New("Config file Not Found")
		return err
	}
	appConfig.Token = viperC.GetString(apiTokenString)
	return nil
}

//SaveViperConfig saves Viper's configurations back to the file
func SaveViperConfig() error {
	filename := cfgPath + cfgFileWithExt
	f, err := os.Create(filename)
	f.Chmod(os.FileMode(0600))
	if err != nil {
		fmt.Println(err.Error())
		return err
	}
	defer f.Close()
	var c config
	c = appConfig
	if err != nil {
		fmt.Println(err.Error())
		return err
	}
	if c.Token == "" {
		return errors.New("API key is empty")
	}
	buf, err := json.Marshal(c)
	if err != nil {
		fmt.Println(err.Error())
		return err
	}
	_, err = f.WriteString(string(buf))
	return err
}
